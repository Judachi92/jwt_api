package com.uniajc.apiRestBackend.rest;


import com.uniajc.apiRestBackend.model.Persona;
import com.uniajc.apiRestBackend.repo.IPersonaRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/personas")
@Api(value="Sistema administrativo de Personas", description="Operaciones habilitadas para Personas en el sistema administrativo")
public class RestPersonaController {
	
	@Autowired
	private IPersonaRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de personas del sistema SAP", response = Persona.class)
	public List<Persona> listar(){
		return repo.findAll();
	}
	
	/*@GetMapping(value = "/{id}")
	@ApiOperation(value = "Listado de personas del sistema", response = Persona.class)
	public Optional<Persona> mostrar(@PathVariable("id") Integer id){
		return repo.findById(id);
	}*/
	
	@PostMapping
	@ApiOperation(value = "Crear una nueva Persona")
	public void insertar(@RequestBody Persona persona){
		repo.save(persona);
	}
	
	@PutMapping
	@ApiOperation(value = "Actualizar una persona")
	public void modificar(@RequestBody Persona persona){
		repo.save(persona);
	}
	
	/*@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Borrar una Persona")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}*/
}
