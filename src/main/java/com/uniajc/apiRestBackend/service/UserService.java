package com.uniajc.apiRestBackend.service;

import com.uniajc.apiRestBackend.model.User;
import com.uniajc.apiRestBackend.repo.IUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired
	private final
    IUserRepo userRepo;

    public UserService(IUserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepo.findAll().forEach(users::add);
        return users;
    }

    /*public User getUserById(long id) {
        return userRepo.findById(id).get();
    }*/

    public User getUserByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    public void saveOrUpdate(User user) {
    	userRepo.save(user);
    }

    /*public void delete(long id) {
    	userRepo.deleteById(id);
    }*/
}