package com.uniajc.apiRestBackend.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "usuarios")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @ApiModelProperty(notes = "name of the User")
    @Column(name = "username")
    private String username;

    @ApiModelProperty(notes = "email of the User")
    @Column(name = "email")
    private String email;

    @ApiModelProperty(notes = "password of the User")
    @Column(name = "password")
    private String password;

    @ApiModelProperty(notes = "Token for security requests")
    @Column(name = "jwt")
    private String jwt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", email=" + email + ", password=" + password + ", jwt="
                + jwt + "]";
    }


}
